FROM bitnami/cassandra:latest
EXPOSE 9042 7000
USER 1001
ENTRYPOINT [ "/app-entrypoint.sh" ]
CMD [ "/run.sh" ]